import cv2
import numpy as np
import matplotlib.pyplot  as plt
#108 300
def empty(a) :
    pass

cv2.namedWindow('image')
cv2.resizeWindow('image',640,240)
#track-bars
cv2.createTrackbar('MIN-VAL','image', 0, 300, empty)
cv2.createTrackbar('MAX-VAL','image',0, 300, empty)

# while True:





def ROI1(image):
    height = image.shape[0]
    triangle = np.array([
        [(200, height), (1150, height), (700, 400)]
    ])
    mask = np.zeros_like(image)
    cv2.fillPoly(mask, triangle, 255)
    return mask

def ROI2(image):
    height = image.shape[0]
    triangle = np.array([
        [(0, 650), (1050, height), (750, 400)]
    ])
    mask = np.zeros_like(image)
    cv2.fillPoly(mask, triangle, 255)
    return mask


cap = cv2.VideoCapture("challenge_video.mp4")


def  avergaeLines(image, linez):
    left_fit = []
    right_fit = []

    for line in linez:
        x1, y1, x2, y2 = line.reshape(4)
        parameters = np.polyfit((x1, x2), (y1, y2), 1)
        slope = parameters[0]
        intercept = parameters[1]
        if slope < 0:
             left_fit.append((slope, intercept))
        else:
            right_fit.append((slope, intercept))
    left_Fit_Average = np.average(left_fit, axis=0)
    right_Fit_Average = np.average(right_fit, axis=0)
    left_line = coord(image, left_Fit_Average)
    right_line = coord(image, right_Fit_Average)
    return np.array([left_line, right_line])
#img = cv2.imread("test3.jpg")


def displayLines(image, lines):
    line_image = np.zeros_like(image)
    if lines is not None:
        for line in lines:
            x1, y1, x2, y2 = line.reshape(4)
            cv2.line(line_image, (x1, y1), (x2, y2), (0, 255, 0), 10)
    return line_image

def coord(image, linePara):
    try:
        slope, intercept = linePara
    except TypeError:
        slope, intercept = 0.001, 0
    y1 = image.shape[0]
    y2 = int(y1*(4/5))
    x1 = int((y1 - intercept)/slope)
    x2 = int((y2-intercept)/slope)
    return np.array([x1, y1, x2, y2])


while True:
    success, img = cap.read()
    while True:
        lane_image = np.copy(img)
        # cv2.imshow("a", lane_image)
        gray = cv2.cvtColor(lane_image, cv2.COLOR_RGB2GRAY)
        # cv2.imshow("b", gray)
        blur_gray = cv2.GaussianBlur(gray, (5, 5), 0)
        # cv2.imshow("c", blur_gray)
        min_val = cv2.getTrackbarPos('MIN-VAL', 'image')
        max_val = cv2.getTrackbarPos('MAX-VAL', 'image')
        canny_blur_gray = cv2.Canny(blur_gray, 0, 188)
        # cv2.imshow("f", canny_blur_gray)
        mask = ROI2(canny_blur_gray)
        ADDED = cv2.bitwise_and(canny_blur_gray, mask)
        lines = cv2.HoughLinesP(ADDED, 2, np.pi/180, 100, np.array([]), minLineLength=70, maxLineGap=4)
        #70 AND 7 WORKS FINE
        averagedLines = avergaeLines(lane_image, lines)
        linesImage = displayLines(lane_image, averagedLines)
        comboImage = cv2.addWeighted(lane_image, 0.8, linesImage, 1, 1)
        key = cv2.waitKey(100)
        cv2.waitKey(0) & 0xFF

        cv2.imshow("RESULT", comboImage)
        if cv2.waitKey(0) & 0xFF == ord('c'):
            break





        #188 but 209 seems perfect
    # print(min_wal, max_val)
    #ROI_image = ROI(canny_blur_gray)




